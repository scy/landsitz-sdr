from datetime import datetime
import json
from os import environ
from time import time
from typing import ClassVar, List, Optional, Union
try:
    from typing import Literal  # new in 3.8
except ImportError:
    from typing_extensions import Literal

from pydantic import BaseModel, Field, ValidationError, parse_raw_as, validator
import requests


class FrequencySetup(BaseModel):
    center_frequency: int
    frequencies: List[int]
    hop_times: List[int]

    class Config:
        allow_mutation = False


class SensorData(BaseModel):
    INTERVAL: ClassVar[int] = 12

    time: datetime
    model: str
    id: int

    mod: Optional[str]
    mic: Optional[str]
    freq1: Optional[float]
    freq2: Optional[float]
    rssi: Optional[float]
    noise: Optional[float]
    snr: Optional[float]

    class Config:
        allow_mutation = False

    def name_path(self):
        return []

    def to_graphite(self):
        return []

    def to_graphite_value(self, v):
        if isinstance(v, bool):
            return 1 if v else 0
        return v


class Bresser5in1Data(SensorData):
    EXPORT_FIELDS: ClassVar[List[str]] = [
        "bat_ok", "temp_c", "hum_pct", "wind_avg_mps", "wind_max_mps",
        "wind_dir_deg", "rain_mm", "snr",
    ]
    model: Literal["Bresser-5in1"]

    bat_ok: Optional[bool] = Field(None, alias="battery_ok")
    temp_c: float = Field(..., alias="temperature_C")
    hum_pct: int = Field(..., alias="humidity")
    wind_avg_mps: float = Field(..., alias="wind_avg_m_s")
    wind_max_mps: float = Field(..., alias="wind_max_m_s")
    wind_dir_deg: float
    rain_mm: float

    def name_path(self):
        return ["weather"]

    def to_graphite(self):
        now = round(time())
        return [{
            "name": ".".join(self.name_path() + [field]),
            "value": self.to_graphite_value(getattr(self, field)),
            "interval": self.INTERVAL,
            "time": now,
        } for field in self.EXPORT_FIELDS]


class WMBusData(SensorData):
    INTERVAL: ClassVar[int] = 450  # 7.5 minutes

    model: Literal["Wireless-MBus"]
    type: int
    type_string: str

    mode: Optional[str]
    M: Optional[str]
    version: Optional[int]

    def name_path(self):
        return ["metering"]

    def to_graphite(self):
        now = round(time())
        name_path = self.name_path()
        return [{
            "name": ".".join(name_path + ["consumption", str(self.id)]),
            "value": self.to_graphite_value(self.current_value),
            "interval": self.INTERVAL,
            "time": now,
        }, {
            "name": ".".join(name_path + ["snr", str(self.id)]),
            "value": self.to_graphite_value(self.snr),
            "interval": self.INTERVAL,
            "time": now,
        }]


class WMBusHCAData(WMBusData):
    type: Literal[8]

    current_value: int = Field(..., alias="inst_hca_0")

    @validator("current_value", pre=True)
    def parse_value(cls, v):
        # v is usually something like "123.000".
        return round(float(v))

    def name_path(self):
        return super().name_path() + ["hca"]


class WMBusWaterData(WMBusData):
    type: Literal[6, 7, 55]

    current_value: float = Field(..., alias="inst_volume_0")

    @validator("current_value", pre=True)
    def parse_value(cls, v):
        # v is usually something like "2.690 m3".
        if not v.endswith(" m3"):
            raise ValueError('expected " m3" suffix')
        return float(v[:-3])

    def name_path(self):
        return super().name_path() + ["water"]


KnownPacket = Union[FrequencySetup, Bresser5in1Data, WMBusHCAData, WMBusWaterData]


def parse_line(line: str) -> BaseModel:
    return parse_raw_as(KnownPacket, line)


def handle_line(line: str):
    try:
        obj = parse_line(line)
    except ValidationError:
        print(f"Could not parse: {line}")
        return
    if isinstance(obj, SensorData):
        data = obj.to_graphite()
        print(data)
        graphite_publish(data)


def graphite_publish(values):
    try:
        res = requests.post(
            environ["GRAPHITE_URL"],
            headers={
                "Authorization": f"Bearer {environ['GRAPHITE_APIKEY']}",
            },
            json=values,
        )
        print(res, res.content)
    except:
        return


if __name__ == "__main__":
    from sys import stdin
    for line in stdin:
        handle_line(line)
