#!/bin/sh
set -e

rtl_433 -f 868.625M -R 104 -R 119 -s 1M -Y autolevel -Y magest -M noise -M level -Y filter=20 -F json | python3 watch_rtl.py
