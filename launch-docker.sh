#!/bin/sh
set -e

rtl_path="$(lsusb -d 0bda:2838 | awk '{ print "/dev/bus/usb/" $2 "/" $4 }' | tr -d :)"

docker run --device "$rtl_path" hertzg/rtl_433:alpine-21.05 -f 868.625M -R 104 -R 119 -s 1M -Y autolevel -Y magest -M noise -M level -Y filter=20 -F json | python3 watch_rtl.py
